"""
DESAFIO 081: Extraindo Dados de uma Lista

Crie um programa que vai ler vários números e colocar em uma lista. Depois
disso, mostre:

A) Quantos números foram digitados.
B) A lista de valores, ordenada de forma decrescente.
C) Se o valor 5 foi digitado e está ou não na lista.
"""

from random import randrange

alea_nums = [randrange(0, 15) for i in range(9)]

# alea_nums =[]
# while True:
#     alea_nums.append(int('Digite um número: '))
#     continua = str(input('Deseja continuar [S/N]? '))
#     if continua in 'nN':
#         break


print(f'A lista tem {len(alea_nums)} números')
print(f'Contendo o número 5' if 5 in alea_nums else 'NÃO contendo o número 5')
alea_nums.sort()
alea_nums.reverse()
print(f'Descrescente: {alea_nums}')
