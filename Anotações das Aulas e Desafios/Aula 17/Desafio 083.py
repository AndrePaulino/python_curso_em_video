"""
DESAFIO 083: Validando Expressões Matemáticas

Crie um programa onde o usuário digite uma expressão qualquer que use
parênteses. Seu aplicativo deverá
analisar se a expressão passada está com os parênteses abertos e fechados na
ordem correta.
"""

stack = []

frase = str(input('Digite uma frase com parentêses: '))

for char in frase:
    if char == '(':
        stack.append(char)
    elif char == ')':
        if len(stack) > 0:
            stack.pop()
        else:
            stack.append(char)

if len(stack) > 0:
    print('Parentêses inválido.')
else:
    print('Parentêses válido.')
