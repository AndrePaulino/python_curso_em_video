"""
DESAFIO 078: Maior e Menor Valores na Lista

Faça um programa que leia 5 valores numéricos e guarde-os em uma lista. No final, mostre
qual foi o maior e o menor valor digitado e as suas respectivas posições na lista.
"""

from random import randrange

alea_nums = [randrange(0, 50) for i in range(10)]

# alea_nums = []
# for i in range(5):
#     nums.append(int(input('Digite um número: ')))

maior = max(alea_nums)
menor = min(alea_nums)

print(alea_nums)
print(f'{maior} é o maior número e esta na posição {alea_nums.index(maior) + 1}\n{menor} é o menor e esta na posição {alea_nums.index(menor) + 1}')
