"""
DESAFIO 080: Lista Ordenada sem Repetições

Crie um programa onde o usuário possa digitar cinco valores numéricos e
cadastre-os em uma lista,
já na posição correta de inserção (sem usar o sort()). No final, mostre a lista
ordenada na tela.
"""

from random import randrange

sep = '◄►' * 20
alea_nums = []

for i in range(10):
    # num_tmp = int(input('Digite um número: '))
    num_tmp = randrange(0, 11)

    if i == 0 or num_tmp > alea_nums[-1]:
        alea_nums.append(num_tmp)
        print(f'{num_tmp} adicionado no final')
        continue

    if num_tmp not in alea_nums:
        for ii, num in enumerate(alea_nums):
            if num_tmp < num:
                alea_nums.insert(ii, num_tmp)
                print(f'{num_tmp} adicionado na {ii + 1}ª posição')
                break

print(sep)
print(f'Em ordem crescente: {alea_nums}')
