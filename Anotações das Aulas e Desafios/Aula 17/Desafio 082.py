"""
DESAFIO 082: Dividindo Valores em Várias Listas

Crie um programa que vai ler vários números e colocar em uma lista. Depois
isso, cria duas listas extras que vão
conter apenas os valores pares e os valores ímpares digitados, respectivamente.
Ao final, mostre o conteúdo das
três listas geradas.
"""

from random import randrange

alea_nums = [randrange(0, 50) for i in range(9)]
pares = [i for i in alea_nums if i % 2 == 0]
impares = [i for i in alea_nums if i % 2 != 0]

# alea_nums =[]
# while True:
#     alea_nums.append(int('Digite um número: '))
#     continua = str(input('Deseja continuar [S/N]? '))
#     if continua in 'nN':
#         break

print(f'A lista: {alea_nums}\nSeus pares: {pares}\nSeus ímpares: {impares}')
