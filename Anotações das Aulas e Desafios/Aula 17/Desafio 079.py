"""
DESAFIO 079: Valores Únicos em uma Lista

Crie um programa onde o usuário possa digitar vários valores numéricos e
cadastre-os em uma lista.
Caso o número já exista lá dentro, ele não será adicionado. No final, serão
exibidos todos os
valores únicos digitados, em ordem crescente.
"""

from random import randrange

sep = '═' * 40
alea_nums = []

for i in range(10):
    # num_tmp = int(input('Digite um número: '))
    num_tmp = randrange(0, 11)
    if num_tmp not in alea_nums:
        alea_nums.append(num_tmp)
    else:
        print(f'{num_tmp} já existe, não será adicionado.')
alea_nums.sort()

print(sep)
print(f'A lista final:\n{alea_nums}')
print(sep)
