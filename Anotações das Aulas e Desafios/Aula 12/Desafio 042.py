"""
DESAFIO 042: Analisando Triângulos v2.0

Refaça o DESAFIO 035 dos triângulos, acrescentando o recurso de mostrar que tipo de triângulo será formado:

- Equilátero: todos os lados iguais
- Isósceles: dois lados iguais
- Escaleno: todos os lados diferentes
"""

print('Digite o comprimento de três retas: ')
r1 = float(input('Reta 1: '))
r2 = float(input('Reta 2: '))
r3 = float(input('Reta 3: '))

if r1 + r2 >= r3 and r1 + r3 >= r2 and r2 + r3 >= r1:
	if r1 == r2 == r3:
		print('Estas retas FORMAM um triângulo EQUILÁTERO')
	elif r1 != r2 != r3 != r1:
		print('Estas retas FORMAM um triângulo ESCALENO')
	else:
		print('Estas retas FORMAM um triângulo ISÓSCELES')
else:
	print('Estas retas NÃO FORMAM um triângulo')
