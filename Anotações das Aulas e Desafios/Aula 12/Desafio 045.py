"""
DESAFIO 045: Pedra, Papel e Tesoura

Crie um programa que faça o computador jogar Jokenpô com você.
"""

from random import randint
from time import sleep

itens = ('Pedra', 'Papel', 'Tesoura')

print('''JOKENPÔ
[ 0 ] PEDRA
[ 1 ] PAPEL
[ 2 ] TESOURA''')
player = int(input('Sua jogada: '))
pc_mao = randint(0, 2)
vencedor = ''

if player != 0 and player != 1 and player != 2:
	print('JOGADA INVÁLIDA, TENTE NOVAMENTE')
else:
	if pc_mao == 0:                 # Computador PEDRA
		if player == 1:
			vencedor = 'JOGADOR'
		elif player == 2:
			vencedor = 'COMPUTADOR'
		else:
			vencedor = 'NINGUÉM'
	elif pc_mao == 1:               # Computador PAPEL
		if player == 2:
			vencedor = 'JOGADOR'
		elif player == 0:
			vencedor = 'COMPUTADOR'
		else:
			vencedor = 'NINGUÉM'
	else:                           # Computador TESOURA
		if player == 0:
			vencedor = 'JOGADOR'
		elif player == 1:
			vencedor = 'COMPUTADOR'
		else:
			vencedor = 'NINGUÉM'

print('\033[1;34mJO')
sleep(1)
print('KEN')
sleep(1)
print('PÔ!!!\033[m')
print('-=' * 15)
print('Jogador jogou: {}\nComputador jogou: {}\n\033[1;32m{} VENCEU!'.format(itens[player], itens[pc_mao], vencedor))
