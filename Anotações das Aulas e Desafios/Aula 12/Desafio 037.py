"""
DESAFIO 037: Conversor de Bases Numéricas

Escreva um programa que leia um número inteiro qualquer e peça para o usuário escolher qual será
a base de conversão:

- 1 para Binário
- 2 para Octal
- 3 para Hexadecimal
"""

num = int(input('Digite um número inteiro: '))
print('''Escolha uma das bases de conversão:
[ 1 ] BINÁRIO
[ 2 ] OCTAL
[ 3 ] HEXADECIMAL''')
escolha = int(input('Sua escolha: '))

if escolha == 1:
	print('{} em BINÁRIO é: {}'.format(num, bin(num)[2:]))
elif escolha == 2:
	print('{} em OCTADECIMAL é: {}'.format(num, oct(num)[2:]))
elif escolha == 3:
	print('{} em HEXADECIMAL é: {}'.format(num, hex(num)[2:]))
else:
	print('Opção inválida, tente novamente')
