"""
DESAFIO 041: Classificando Atletas

A Confederação Nacional de Natação precisa de um programa que leia o ano
de nascimento de um atleta e mostre sua categoria, de acordo com a idade:

- Até 9 anos: MIRIM
- Até 14 anos: INFANTIL
- Até 19 anos: JUNIOR
- Até 25 anos: SÊNIOR
- Acima: MASTER
"""

from datetime import date

nasc = int(input('Digite o ano de nascimento do atleta: '))
idade = (date.today().year) - nasc
categ = ''

if idade <= 9:
	categ = 'MIRIM'
elif idade <= 14:
	categ = 'INFANTIL'
elif idade <= 19:
	categ = 'JUNIOR'
elif idade <= 25:
	categ = 'SÊNIOR'
else:
	categ = 'MASTER'

print('Com {} anos, esta na categoria {}'.format(idade, categ))