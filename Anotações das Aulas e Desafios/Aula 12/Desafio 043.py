"""
DESAFIO 043: Índice de Massa Corporal

Desenvolva uma lógica que leia o peso e a altura de uma pessoa, calcule seu IMC
e mostre seu status, de acordo com a tabela abaixo:

- Abaixo de 18.5: Abaixo do Peso
- Entre 18.5 e 25: Peso Ideal
- 25 até 30: Sobrepeso
- 30 até 40: Obesidade
- Acima de 40: Obesidade Mórbida
"""

peso = float(input('Digite o peso (kg): '))
alt = float(input('Digite a altura (cm)'))
imc = peso / ((alt * .01) ** 2)
status = ''

if imc <= 18.5:
	status = 'Abaixo do peso'
elif  25 >= imc > 18.5:
	status = 'Peso ideal'
elif 30 >= imc > 25:
	status = 'Sobrepeso'
elif 40 >= imc > 30:
	status = 'Obesidade'
else:
	status = 'Obesidade mórbida'

print('IMC: {:.1f} tem o status de {}'.format(imc, status))