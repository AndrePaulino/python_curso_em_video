"""
DESAFIO 036: Aprovando Empréstimo

Escreva um programa para aprovar o empréstimo bancário para a compra de uma casa.
Pergunte o valor da casa, o salário do comprador e em quantos anos ele vai pagar.
A prestação mensal não pode exceder 30% do salário, ou então o empréstimo será negado.
"""

casa = float(input('Qual o valor da casa? '))
sal = float(input('Qual o salário do comprador? '))
anos = float(input('Em quantos anos pagará? '))

prestacao = casa / (anos * 12)
aprovado = True if prestacao <= (sal * .3) else False

print('Para uma casa de R$ {:.2f}, em {} anos a prestação sera de {:.2f}'.format(casa, anos, prestacao))
if aprovado:
	print('Empréstimo CONCEDIDO')
else:
	print('Empréstimo NEGADO')
