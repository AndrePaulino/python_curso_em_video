"""
DESAFIO 039: Alistamento Militar

Faça um programa que leia o ano de nascimento de um jovem e informe, de acordo com sua idade,
se ele ainda vai se alistar ao serviço militar, se é a hora de se alistar, ou se já passou
do tempo do alistamento. Seu programa também deverá mostrar o tempo que falta ou que passou do prazo.
"""

from datetime import date

nasc = int(input('Digite o ano em que nasceu: '))
idade = (date.today().year) - nasc

if idade > 18:
	print('Passou da hora de se alistar, procure uma Junta Militar e se REGULARIZE!\nVocê está atrasado {} anos'.format(
		idade - 18))
elif idade < 18:
	print('Não está na hora, faltam {} anos'.format(18 - idade))
else:
	print('Está na hora, ALISTE-SE!')
