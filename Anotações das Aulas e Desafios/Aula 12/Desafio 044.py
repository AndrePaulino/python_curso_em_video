"""
DESAFIO 044: Gerenciador de Pagamentos

Elabore um programa que calcule o valor a ser pago de um produto,
considerando o seu preço normal, e condição de pagamento:

- À vista no dinheiro/cheque: 10% de desconto
- À vista no cartão: 5% de desconto
- Em até 2x no cartão: preço normal
- 3x ou mais no cartão: 20% de juros
"""

preco = float(input('Preço total das compras: '))
print('''FORMAS DE PAGAMENTO
[ 1 ] À vista no dinheiro/cheque
[ 2 ] À vista no cartão
[ 3 ] 2x no cartão
[ 4 ] 3x ou mais no cartão''')
escolha = int(input('Qual é sua escolha? '))

if escolha == 1:
	total = preco - (preco * .1)
elif escolha == 2:
	total = preco - (preco * .5)
elif escolha == 3:
	total = preco
	parcela = total / 2
	print('Sua compra sera parcelada em 2x de R${:.2f} SEM JUROS'.format(parcela))
elif escolha == 4:
	total =  preco + (preco * .2)
	tot_parc = int(input('Quantas vezes? '))
	parcela = total / tot_parc
	print('Sua compra sera parcelada em 3x de R${:.2f} COM JUROS'.format(parcela))
else:
	total = preco
	print('Opção de pagamento inválida. Tente novamente!')

print('O preço final de sua compra é de R${:.2f}'.format(total))