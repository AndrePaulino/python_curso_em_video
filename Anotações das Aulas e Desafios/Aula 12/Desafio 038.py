"""
DESAFIO 038: Comparando Números

Escreva um programa que leia dois números inteiros e compare-os, mostrando na tela uma mensagem:

- O primeiro valor é maior.
- O segundo valor é maior.
- Não existe valor maior, os dois são iguais.
"""

print('Digite dois números: ')
a = int(input('A: '))
b = int(input('B: '))

print('{} é o maior entre eles'.format(max(a, b)))
