"""
DESAFIO 088: Palpites Para a Mega Sena

Faça um programa que ajude um jogador da MEGA SENA a criar palpites. O programa vai perguntar quantos jogos
serão gerados e vai sortear 6 números entre 1 e 60 para cada jogo, cadastrando tudo em uma lista composta.
"""

from random import randrange, sample

sorteio = sample(range(1, 60), 6)
jogos_ms = []
sep = '—' * 30

quant_jogos = int(input('Quantos jogos de MEGA SENA deseja fazer? '))

for i in range(1, quant_jogos + 1):
    jogo = [randrange(1, 60 + 1) for i in range(6)]

    # jogo = []
    # print(f'{sep}\n{i:>12}ª JOGO\n{sep}')
    # for ii in range(1, 6 + 1):
    #     dez_tmp = int(input(f'Insira a {ii}ª dezena: '))
    #     jogo.append(dez_tmp)

    jogos_ms.append(jogo)


print(f'{sep}\nJogo sorteado: {sorteio}\n{sep}\nSeus jogos:')
for i in range(len(jogos_ms)):
    acerto = set(jogos_ms[i]).intersection(sorteio)
    print(f'{jogos_ms[i]} | Acertos: ', end='')
    print('Nenhum' if not acerto else acerto)
