"""
DESAFIO 086: Matriz em Python

Crie um programa que crie uma matriz de dimensão 3x3 e preencha com valores lidos pelo teclado.

0 [_][_][_]
1 [_][_][_]
2 [_][_][_]
   0  1  2

No final, mostre a matriz na tela, com a formatação correta.
"""

from random import randrange

matriz = [[0] * 3, [0] * 3, [0] * 3]

sep = '▄' * 30
cont = 0
for l in range(len(matriz[0])):
    for c in range(len(matriz)):
        # num_tmp = int(input(f'Digite [{l},{c}] '))
        # matriz[l][c] = num_tmp

        matriz[l][c] = randrange(0, 101)
        cont += 1

print(sep)
print(f'A matriz formada:\n{matriz[0]}\n{matriz[1]}\n{matriz[2]}')
