"""
DESAFIO 085: Listas com Pares e Ímpares

Crie um programa onde o usuário possa digitar sete valores numéricos e cadastre-os em uma lista única que mantenha
separados os valores pares e ímpares. No final, mostre os valores pares e ímpares em ordem crescente.
"""

from random import randrange

alea_nums = [randrange(0, 101) for i in range(0, 10)]
nums_ord = [[i for i in alea_nums if i % 2 == 0], [i for i in alea_nums if i % 2 != 0]]
sep = '═' * 40

# nums_ord = [[], []]
# for i in range(7):
#     num_tmp = int(input('Digite um número: '))
#     if num_tmp % 2 == 0:
#         nums_ord[0].append(num_tmp)
#     else:
#         nums_ord[1].append(num_tmp)

nums_ord[0].sort()
nums_ord[1].sort()
print(sep)
print(f'Os números pares: {nums_ord[0]}\nOs números ímpares: {nums_ord[1]}')
