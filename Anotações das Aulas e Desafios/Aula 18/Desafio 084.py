"""
DESAFIO 084: Lista Composta e Análise de Dados

Faça um programa que leia nome e peso de várias pessoas, guardando tudo em uma lista. No final, mostre:

A) Quantas pessoas foram cadastradas.
B) Uma listagem com as pessoas mais pesadas.
C) Uma listagem com as pessoas mais leves.
"""

from random import uniform

alea_peso = [round(uniform(35, 150), 2) for i in range(9)]

# sep = '→←­­­' * 10
# alea_peso = []
# cont = 0
# while True:
#     cont += 1
#     print(f'{sep}\n{cont:>20}ª PESSOA\n{sep}')

#     alea_peso.append(float(input('Digite o peso da pessoa (kg): ')))
#     continua = str(input('Deseja continuar [S/N]? '))
#     if continua in 'nN':
#         break

alea_peso.sort()
pesadas = alea_peso[-3:]
leves = alea_peso[:3]

print(f'{len(alea_peso)} pessoas foram cadastradas\nAs mais pesadas tem (kg): {pesadas}\nAs mais leves (kg): {leves}')
