"""
DESAFIO 089: Boletim com Listas Compostas

Crie um programa que leia nome e duas notas de vários alunos e guarde tudo em uma lista composta.
No final, mostre um boletim contendo a média de cada um e permita que o usuário possa mostrar as
notas de cada aluno individualmente.
"""

from random import uniform, choices
import string

turma_boletim = []
quant_alunos = 30
sep, sep_1 = '—' * 30, '-' * 30
quant_alunos = int(input('Quantos alunos irá cadastrar? '))

# for aluno in range(quant_alunos):
#     nome_aluno = str(input('Nome do aluno: ')).strip().capitalize()
#     n1 = float(input('Nota 1: '))
#     n2 = float(input('Nota 2: '))
#     media = (n1 + n2) / 2
#     print(sep)
#     aluno_boletim = [nome_aluno, media, [n1, n2]]
#     turma_boletim.append(aluno_boletim)

for aluno in range(quant_alunos):
    nome_aluno = ''.join(choices(string.ascii_lowercase, k=6)).capitalize()
    n1 = round(uniform(0, 10), 1)
    n2 = round(uniform(0, 10), 1)
    media = round(((n1 + n2) / 2), 1)
    aluno_boletim = [nome_aluno, media, [n1, n2]]
    turma_boletim.append(aluno_boletim)


for i, aluno in enumerate(turma_boletim):
    print(f'{sep}\nNº: {i}\nAluno: {aluno[0]}\nMédia: {aluno[1]}')

while True:
    print(sep_1)
    nota = int(input('Digite 999 para cancelar.\nGostaria de ver as notas de qual aluno? [Nº] '))
    if nota == 999:
        break

    print(sep_1)
    print(f'Notas aluno {turma_boletim[nota][0]}: {turma_boletim[nota][2]}')
