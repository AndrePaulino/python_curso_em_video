"""
DESAFIO 087: Mais Sobre Matriz em Python

Aprimore o desafio anterior, mostrando no final:

A) A soma de todos os valores pares digitados.
B) A soma dos valores da terceira coluna.
C) O maior valor da segunda linha.

0 [_][_][_]
1 [_][_][_]
2 [_][_][_]
   0  1  2
"""

from random import randrange

matriz = [[0] * 3, [0] * 3, [0] * 3]
soma_pares, soma_col_3 = 0, 0
sep = '▄' * 30

for l in range(len(matriz[0])):
    for c in range(len(matriz)):
        # num_tmp = int(input(f'Digite [{l},{c}] '))
        # matriz[l][c] = num_tmp

        matriz[l][c] = randrange(0, 101)
        if matriz[l][c] % 2 == 0:
            soma_pares += matriz[l][c]
        if c == 2:
            soma_col_3 += matriz[l][c]

max_lin_2 = max(matriz[1])

print(sep)
print(f'A matriz formada:\n{matriz[0]}\n{matriz[1]}\n{matriz[2]}')
print(sep)
print(f'A soma dos valores dessa matriz: {soma_pares}\nA soma da 3ª coluna: {soma_col_3}\nO maior número da linha 2: {max_lin_2}')
