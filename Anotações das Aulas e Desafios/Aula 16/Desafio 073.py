"""
DESAFIO 073: Tuplas com Times de Futebol

Crie uma tupla preenchida com os 20 primeiros colocados da Tabela
do Campeonato Brasileiro de Futebol, na ordem de colocação.
Depois mostre:

A) Os 5 primeiros.
B) Os últimos 4 colocados.
C) Times em ordem alfabética.
D) Em que posição está o time da Chapecoense.
"""

times = ('Flamengo', 'Cruzeiro', 'Grêmio', 'São Paulo', 'Internacional',
         'Sport Recife', 'Palmeiras', 'Corinthians', 'Fluminense', 'Atlético',
         'América-MG', 'Botafogo', 'Vasco da Gama', 'Chapecoense', 'Santos',
         'Atlético-PR', 'EC Vitória', 'Bahia', 'Paraná', 'Ceará SC')

print(f'Os times do Brasileirão:\n{times}')
print('""' * 20)
print(f'Os primeiros 5 colocados do campeonato:\n{times[:5]}')
print('""' * 20)
print(f'Os últimos 4 colocados do campeonato:\n{times[-4:]}')
print('""' * 20)
print(f'Os times em ordem alfabética:\n{sorted(times)}')
print('""' * 20)
print(f'Grêmio esta na {times.index("Grêmio")} colocação')
