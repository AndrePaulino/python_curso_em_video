"""
DESAFIO 075: Análise de Dados em uma Tupla

Desenvolva um programa que leia quatro valores pelo teclado e guarde-os em uma tupla. No final, mostre:

A) Quantas vezes apareceu o valor 9.
B) Em que posição foi digitado o primeiro valor 3.
C) Quais foram os números pares.
"""

from random import randrange

tupla = tuple([randrange(0, 10) for i in range(10)])
# input('Digite uma lista de números separados por espacço:')
# tupla = tuple(int(' '.join(i)) for i in input('salve: ').split())

print(tupla)
print('**' * 15)
print(f'Número 9 apareceu: {tupla.count(9)} vezes\n3 esta na posição {tupla.index(3) + 1}\nOs números pares: ', end='')

for i in tupla:
    if i % 2 == 0:
        print(f'{i} ', end='')
