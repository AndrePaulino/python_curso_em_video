"""
DESAFIO 074: Maior e Menor Valores em Tupla

Crie um programa que vai gerar cinco números aleatórios e colocar em uma tupla.
Depois disso, mostre a listagem de números gerados e também indique o menor e o
maior valor que estão na tupla.
"""

from random import randrange

tupla_alea = tuple([randrange(0, 100) for i in range(10)])

print(f'Tupla gerada aleatoriamente:\n{tupla_alea}\nMaior número: {max(tupla_alea)}\nMenor número: {min(tupla_alea)}')
