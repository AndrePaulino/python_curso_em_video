"""
DESAFIO 076: Lista de Preços com Tupla

Crie um programa que tenha uma tupla única com nomes de produtos e seus respectivos preços,
na sequência. No final, mostre uma listagem de preços, organizando os dados em forma tabular.
"""

# TODO: Subtituir tuple por dict

from prettytable import PrettyTable

dados = ('Lápis', 1.75,
         'Borracha', 2,
         'Caderno', 15.9,
         'Estojo', 25,
         'Transferidor', 4.20,
         'Compasso', 9.99,
         'Mochila', 120.32,
         'Canetas', 22.30,
         'Livro', 34.90)
tabela = PrettyTable(['Produto', 'Preço'])

prod_tmp = ''
preco_tmp = 0
for i, prod in enumerate(dados):
    if i % 2 == 0:
        prod_tmp = prod
    else:
        preco_tmp = prod
        tabela.add_row([prod_tmp, preco_tmp])

print(tabela)
