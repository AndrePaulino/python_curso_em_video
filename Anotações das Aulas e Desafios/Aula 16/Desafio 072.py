"""
DESAFIO 072: Número por Extenso

Crie um programa que tenha uma tupla totalmente preenchida com uma contagem por
extenso, de zero até vinte.
Seu programa deverá ler um número pelo teclado (entre 0 e 20) e mostrá-lo por
extenso.
"""
cont = ('zero', 'um', 'dois', 'três', 'quatro', 'cinco', 'seis', 'sete',
        'oito', 'nove', 'dez', 'onze', 'doze', 'treze', 'catorze', 'quinze',
        'dezesseis', 'dezessete', 'dezoito', 'dezenove', 'vinte')

num = int(input('Digite um número de 0 a 20: '))
while num not in range(0, 21):
    num = int(input('Tente novamente. Digite um número de 0 a 20: '))
print(f'{num} por extenso é: {cont[num].capitalize()}')
