"""
DESAFIO 077: Contando Vogais em Tupla

Crie um programa que tenha uma tupla com várias palavras (não usar acentos).
Depois disso, você deve mostrar, para cada palavra, quais são as suas vogais.
"""

palavras = ('aprender', 'programar', 'linguagem', 'python', 'curso', 'gratis',
            'estudar', 'praticar', 'trabalhar', 'mercado', 'programador',
            'futuro')
vogais = 'AEIOU'

for palavra in palavras:
    print(f'\nEm {palavra.upper()} as vogais são: ', end='')
    for letra in palavra:
        if letra.upper() in vogais:
            print(f'{letra.upper()} ', end=' ')
