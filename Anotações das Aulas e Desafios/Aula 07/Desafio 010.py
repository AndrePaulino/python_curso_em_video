"""
DESAFIO 010: Conversor de Moedas

Crie um programa que leia quanto dinheiro uma pessoa tem na carteira
e mostre quantos Dólares ela pode comprar.

Considere U$ 1,00 = R$ 3,27
"""

real = float(input('Quantos reais você possui? '))
print('Considerando U$ 1,00 = R$ 3,27, você pode comprar {:.2f}U$'.format(real / 3.27))
