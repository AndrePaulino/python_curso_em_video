"""
DESAFIO 005: Antecessor e Sucessor

Faça um programa que leia um número inteiro e mostre na tela o seu sucessor e seu antecessor.
"""

num = int(input('Digite um número: '))
print('Seu antecessor: {}\nSeu sucessor: {}'.format(num - 1, num + 1))
