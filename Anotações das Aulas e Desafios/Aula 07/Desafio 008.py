"""
DESAFIO 008: Conversor de Medidas

Escreva um programa que leia um valor em metros e o exiba convertido em centímetros e milímetros.
"""

metro = float(input('Digite um valor em metros: '))

print('{}m equivale a {}c e {}mm'.format(metro, metro * 100, metro * 1000))
