"""
DESAFIO 007: Média Aritmética

Desenvolva um programa que leia as duas notas de um aluno, calcule e mostre a sua média.
"""

print('Digite as notas do aluno:')
n1 = float(input('Nota 1: '))
n2 = float(input('Nota 2: '))
media = (n1 + n2) / 2

print('Com as notas {} e {} sua média é {}'.format(n1, n2, media))
