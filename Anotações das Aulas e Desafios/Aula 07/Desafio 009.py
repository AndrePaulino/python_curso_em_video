"""
DESAFIO 009: Tabuada

Faça um programa que leia um número inteiro qualquer e mostre na tela a sua tabuada.
"""

num = int(input('Digite um número inteiro, para sua tabuada: '))

print('-' * 12)
for i in range(11):
	print('{} x {} = {}'.format(num, i, num * i))

print('-' * 12)
