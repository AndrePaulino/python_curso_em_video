"""
DESAFIO 003.2: Somando Dois Números

Crie um programa que leia dois números e mostre a soma entre eles.
"""

print('Digite dois números: ')
a = int(input('A: '))
b = int(input('B: '))

print('Sua soma é: '.format(a + b))
