"""
DESAFIO 059: Criando um Menu de Opções

Crie um programa que leia dois valores e mostre um menu como o abaixo na tela:

[ 1 ] Somar
[ 2 ] Multiplicar
[ 3 ] Maior
[ 4 ] Novos Números
[ 5 ] Sair do Programa

Seu programa deverá realizar a operação solicitada em cada caso.
"""

from time import sleep

print('Digite dois números')
a = int(input('A: '))
b = int(input('B: '))
# a = 1
# b = 3
op = 0

while op != 5:
    print('''Qual operação gostaria de realizar?
[ 1 ] Somar
[ 2 ] Multiplicar
[ 3 ] Maior
[ 4 ] Novos Números
[ 5 ] Sair do Programa''')
    op = int(input('>>>>>>> Sua escolha? '))

    if op == 1:
        print('{} + {} = {}'.format(a, b, a + b))
    elif op == 2:
        print('{} * {} = {}'.format(a, b, a * b))
    elif op == 3:
        print('{} > {}'.format(max(a, b), min(a, b)))
    elif op == 4:
        print('Digite dois números')
        a = int(input('A: '))
        b = int(input('B: '))
    elif op == 5:
        print('Finalizando...')
        sleep(2)
    else:
        print('Opção inválida, tente novamente')
    print('_ ' * 20)
