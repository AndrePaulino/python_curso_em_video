"""
DESAFIO 063: Sequência de Fibonacci v1.0

Escreva um programa que leia um número n inteiro qualquer e mostre
na tela os n primeiros elementos de uma Sequência de Fibonacci.

Ex: 0 → 1 → 1 → 2 → 3 → 5 → 8
"""

num = int(input('Digite um número: '))
print('{} elementos da sequência de Fibonacci'.format(num))
fn, f1, f2 = 0, 0, 1

for i in range(num):
    fn = f1 + f2
    f1 = f2
    f2 = fn
    print('{} → '.format(fn), end='')
