"""
DESAFIO 062: Super Progressão Aritmética v3.0

Melhore o DESAFIO 061, perguntando para o usuário se ele quer mostrar mais alguns termos.
O programa encerra quando ele disser que quer mostrar 0 termos.
"""

a = int(input('Digite um primeiro termo de uma Progressão aritmetica: '))
r = int(input('Sua razão: '))
print('Sua progressão: ', end='')

cont = 1
termos = 10
while cont <= termos:
    print('{}'.format(a + (cont - 1) * r), end=' ')
    if cont == termos:
        termos += int(input('\nQuantos termos mais gostaria de mostrar? '))
    cont += 1
print('Progressão final com {} termos'.format(termos))
