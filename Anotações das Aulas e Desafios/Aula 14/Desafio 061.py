"""
DESAFIO 061: Progressão Aritmética v2.0

Refaça o DESAFIO 051, lendo o primeiro termo e a razão de uma PA, mostrando
os 10 primeiros termos da progressão usando a estrutura while.
"""

a = int(input('Digite um primeiro termo de uma Progressão aritmetica: '))
r = int(input('Sua razão: '))
print('Sua progressão: ', end='')

cont = 0
while cont < 10:
    cont += 1
    print('{}'.format(a + (cont - 1) * r), end=' ')
