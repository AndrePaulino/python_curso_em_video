"""
DESAFIO 060: Cálculo do Fatorial

Faça um programa que leia um número qualquer e mostre seu fatorial.

Ex: 5! = 5 x 4 x 3 x 2 x 1 = 120
"""
from math import factorial

num = 5
# num = int(input('Digite um número: '))


for_fato = num
for i in range(num - 1, 1, -1):
    for_fato *= i


cont = 0
while_fato = 1
while cont != num:
    cont += 1
    while_fato *= cont
    print('{}! = '.format(num) if cont == 1 else 'x ', end='')
    print('{} '.format(cont), end='')
print('= {}'.format(while_fato))

py_fato = factorial(num)

# print('{}! = {}'.format(num, for_fato))
# print('{}! = {}'.format(num, while_fato))
