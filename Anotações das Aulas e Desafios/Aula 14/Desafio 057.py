"""
DESAFIO 057: Validação de Dados

Faça um programa que leia o sexo de uma pessoa, mas só aceite os valores 'M' ou 'F'.
Caso esteja errado, peça a digitação novamente até ter um valor correto.
"""

sexo = str(input('Informe o sexo [F/M]: ')).strip().upper()
while sexo not in ('F','M'):
	sexo = str(input('Dado inválido, por favor tente novamente: ')).strip().upper()
print('Registrado com sucesso!')
