"""
DESAFIO 065: Maior e Menor Valores

Crie um programa que leia vários números inteiros pelo teclado. No final da execução, mostre a média entre
todos os valores e qual foi o maior e o menor valores lidos. O programa deve perguntar ao usuário se ele
quer ou não continuar a digitar valores.
"""

maior, menor, media, soma = 0, 0, 0, 0

cont, tot_num = 1, 5
while cont <= tot_num:
    num = int(input('Digite um número: '))
    soma += num

    if num > maior or cont == 1:
        maior = num
    elif num < menor or cont == 1:
        menor = num

    if cont == tot_num:
        mais_num = int(input('[1] Continuar\n[2] Terminar\n>>>>> Escolha: '))
        if mais_num == 1:
            tot_num += 5
    cont += 1

media = soma / tot_num
print('A média de todos os números digitados é: {}'.format(media))
