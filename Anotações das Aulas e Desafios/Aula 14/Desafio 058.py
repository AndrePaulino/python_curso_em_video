"""
DESAFIO 058: Jogo da Adivinhação v2.0

Melhore o jogo do DESAFIO 028 onde o computador vai "pensar" em um número entre 0 e 10.
Só que agora o jogador vai tentar adivinhar até acertar, mostrando no final quantos
palpites foram necessários para vencer.
"""

from random import randint

num = randint(0, 10)
palpite = int(input('Tente adivinhar um número, de 0 a 10: '))
tot_palp = 0

while num != palpite:
	tot_palp += 1
	palpite = int(input('Erou! Tenta de novo: '))

print('Acertou mizerávi! Precisou de {} palpites para acertar'.format(tot_palp))
