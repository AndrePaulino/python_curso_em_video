"""
DESAFIO 031: Custo da Viagem

Desenvolva um programa que pergunte a distância de uma viagem em km. Calcule o preço da passagem,
cobrando R$ 0,50 por km para viagens de até 200 km e R$ 0,45 para viagens mais longas.
"""

dist = float(input('Digite a distância, em kilometros, da viagem: '))
preco = dist * .5 if dist <= 200 else dist * .45

print('A viagem custará: {:.2f}R$'.format(preco))
