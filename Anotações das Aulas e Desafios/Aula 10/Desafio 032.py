"""
DESAFIO 032: Ano Bissexto

Faça um programa que leia um ano qualquer e mostre se ele é BISSEXTO.
"""

from datetime import date

ano = int(input('Digite um ano qualquer, 0 para o atual: '))
if 0 == ano:
	ano = date.today().year

if ano % 4 == 0 and ano % 100 != 0 or ano % 400 == 0:
	print('{} é BISSEXTO'.format(ano))
else:
	print('{} NÃO é BISSEXTO'.format(ano))