"""
DESAFIO 033: Maior e Menor Valores

Faça um programa que leia três números e mostre qual é o maior e qual é o menor.
"""

print('Digite três números: ')
a = float(input('A: '))
b = float(input('B: '))
c = float(input('C: '))

maior = c
if a > b and a > c:
	maior = a
elif b > a and b > c:
	maior = b

menor = c
if a < b and a < c:
	menor = a
elif b < a and b < c:
	menor = b

print('O menor valor digitado foi {}.'.format(menor))
print('O maior valor digitado foi {}.'.format(maior))