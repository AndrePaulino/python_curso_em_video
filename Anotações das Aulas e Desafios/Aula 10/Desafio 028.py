"""
DESAFIO 028: Jogo da Adivinhação v1.0

Escreva um programa que faça o computador "pensar" em um número inteiro entre 0 e 5
e peça para o usuário tentar descobrir qual foi o número escolhido pelo computador.
O programa deverá escrever na tela se o usuário venceu ou perdeu.
"""

from random import randint

alea = randint(0, 5)
num = int(input('Tente adivinhar um número, de 0 a 5: '))
print('Acertou miserávi' if alea == num else 'Erou!')
