"""
DESAFIO 030: Par ou Ímpar?

Crie um programa que leia um número inteiro e mostre na tela se ele é PAR ou ÍMPAR.
"""

num = int(input('Digite um número diferente de 0: '))
print('PAR' if num % 2 == 0 else 'ÍMPAR')