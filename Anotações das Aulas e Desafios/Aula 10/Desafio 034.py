"""
DESAFIO 034: Aumentos Múltiplos

Escreva um programa que pergunte o salário de um funcionário e calcule o valor do seu aumento.
Para salários superiores a R$ 1.250,00, calcule um aumento de 10%.
Para inferiores ou iguais, o aumento é de 15%.
"""

sal = float(input('Qual o seu salário: '))
aumento = sal * .10 if sal > 1250 else sal * .15

print('Quem ganhava R$ {:.2f} ganhará R$ {:.2f}'.format(sal, sal + aumento))
