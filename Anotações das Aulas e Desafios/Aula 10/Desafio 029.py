"""
DESAFIO 029: Radar Eletrônico

Escreva um programa que leia a velocidade de um carro. Se ele ultrapassar 80 km/h, mostre uma
mensagem dizendo que ele foi multado. A multa vai custar R$ 7,00 por cada km acima do limite.
"""

vel = float(input('Insira a velocidade do carro: '))

if vel > 80:
	multa = 7 * (vel - 80)
	print('PARADO MELIANTE! Você foi multado em: {.:EF}R$'.format(multa))

print('Diriga com cuidado. Bom dia.')