"""
DESAFIO 050: Soma dos Pares

Desenvolva um programa que leia seis números inteiros e mostre a soma apenas
daqueles que forem pares. Se o valor digitado for ímpar, desconsidere-o.
"""
from random import randint

nums = []
soma = 0

for i in range(6):
	nums.append(int(input('Digite um número: ')))
	# nums.append(randint(0, 100))


for i in nums:
	if i % 2 == 0:
		soma += i

print('A soma dos números pares digitados é: {}'.format(soma))