"""
DESAFIO 052: Números Primos

Faça um programa que leia um número inteiro e diga se ele é ou não um número primo.
"""

num = 1
soma = 0
for i in range(1, num + 1):
	if num % i == 0:
		soma += i

if soma == num + 1:
	print('{} é PRIMO'.format(num))
else:
	print('{} NÃO é PRIMO'.format(num))
