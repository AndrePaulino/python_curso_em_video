"""
DESAFIO 048: Soma Ímpares Múltiplos de Três

Faça um programa que calcule a soma entre todos os números ímpares que
são múltiplos de três e que se encontram no intervalo de 1 até 500.
"""
soma = 0
for i in range(1, 5001):
	if i % 2 != 00 and i % 3 == 0:
		soma += i

print('Soma entre todos ímpares múltiplos de 3 entre 1 e 500: {}'.format(soma))