"""
DESAFIO 056: Analisador Completo

Desenvolva um programa que leia o nome, idade e sexo de 4 pessoas. No final do programa, mostre:

- A média de idade do grupo.
- Qual é o nome do homem mais velho.
- Quantas mulheres têm menos de 20 anos.
"""

import string
from random import choices, choice, randint

nome, idade, sexo = [], [], []
media_idade, maior_idade_m, menor_20_f = 0, 0, 0
velho_m = ''

for i in range(4):
	nome.append(''.join(choices(string.ascii_lowercase, k=5 )))
	idade.append(randint(12, 45))
	sexo.append(choice('MF'))

	print('----- {}ª PESSOA -----'.format(i + 1))
	nome.append(str(input('Nome: ')).strip())
	idade.append(int(input('Idade: ')))
	sexo.append(str(input('Sexo [M/F]:')))

	if sexo[i] in 'Mm':
		if idade[i] > maior_idade_m:
			maior_idade_m = idade[i]
			velho_m = nome[i]
	else:
		if idade[i] < 20:
			menor_20_f += 1


media_idade =  sum(idade) / len(idade)
print('A média de idade do grupo: {}\n{} é  homem mais velho com {} anos\n{} mulheres tem menos de 20 anos'.format(media_idade, velho_m, maior_idade_m, menor_20_f))
