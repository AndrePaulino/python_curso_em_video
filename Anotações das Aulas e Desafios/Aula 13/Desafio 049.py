"""
DESAFIO 049: Tabuada v2.0

Refaça o DESAFIO 009, mostrando a tabuada de um número que
o usuário escolher, só que agora utilizando um laço for.
"""

num = int(input('Digite um número inteiro, para sua tabuada: '))
print('-' * 12)
for i in range(11):
	print('{} x {} = {}'.format(num, i, num * i))

print('-' * 12)