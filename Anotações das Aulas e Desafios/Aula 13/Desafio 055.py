"""
DESAFIO 055: Maior e Menor da Sequência

Faça um programa que leia o peso de cinco pessoas.
No final, mostre qual foi o maior e o menor peso lidos.
"""

from random import uniform

pesos = []

print('Digite o peso de 5 pessoas: ')
for i in range(5):
	# pesos.append(float(input('Digite o peso(kg): ')))
	pesos.append(uniform(50, 110))

print('A pessoa mais leve pesa {:.3f}kg e mais pesada {:.3f}kg'.format(min(pesos), max(pesos)))
