"""
DESAFIO 054: Grupo de Maioridade

Crie um programa que leia o ano de nascimento de sete pessoas. No final, mostre
quantas pessoas ainda não atingiram a maioridade e quantas já são maiores.
"""

from random import randint
from datetime import date

anos = []
atual = date.today().year
maiores = 0
menores = 0

print('Digite o ano de nascimento de 7 pessoas: ')
for i in range(6):
	# anos.appen(int(input('Digite o ano de nascimento: ')))
	anos.append(randint(1970, 2016))
	if (atual - anos[i]) >= 18:
		maiores += 1
	else:
		menores += 1

print('{} pessoas são maiores e {} menores'.format(maiores, menores))
