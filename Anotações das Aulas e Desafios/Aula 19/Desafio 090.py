"""
DESAFIO 090: Dicionário em Python

Faça um programa que leia nome e média de um aluno, guardando também a situação em um dicionário.
No final, mostre o conteúdo da estrutura na tela.
"""
keys_aluno = ['nome', 'media', 'situacão']
aluno = {key: None for key in keys_aluno}

aluno['nome'] = str(input('Nome do aluno: ')).strip().capitalize()
aluno['media'] = float(input('Media do aluno: '))
if aluno['media'] >= 7:
    aluno['situacao'] = 'Aprovado'
elif 7 > aluno['media'] >= 5:
    aluno['situacao'] = 'Recuperação'
else:
    aluno['situacao'] = 'Reprovado'


print(f'{aluno["nome"]} com média {aluno["media"]} está {aluno["situacao"]} ')
