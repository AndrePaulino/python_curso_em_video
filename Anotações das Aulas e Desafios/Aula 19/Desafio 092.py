"""
DESAFIO 092: Dicionário em Python

Crie um programa que leia nome, ano de nascimentoimento e carteira de trabalho e cadastre-os (com idade) em um dicionário se por acaso o CTPS diferente de ZERO, também o ano de contratação e o salário.
Calcule e acrescente, além da idade, com quantos anos a pessoa vai se aposentar.
"""

from datetime import datetime

sep = '÷' * 40
cadastro = dict()

cadastro['nome'] = str(input('Nome: '))
cadastro['nascimento'] = int(input('Ano de nascimentoimento: '))
cadastro['idade'] = datetime.now().year - cadastro['nascimento']
cadastro['ctps'] = int(input('Carteira de Trabalho [0 não tem]: '))
if cadastro['ctps'] != 0:
    cadastro['contratacao'] = int(input('Ano da Contratação: '))
    cadastro['salario'] = float(input('Salário: '))
    cadastro['aposentadoria'] = (
        cadastro['contratacao'] + 35) - cadastro['nascimento']

print(sep)
for key, value in cadastro.items():
    print(f'{key}: {value}')
