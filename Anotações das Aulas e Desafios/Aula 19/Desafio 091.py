"""
DESAFIO 091: Dicionário em Python

Crie um programa onde 4 jogadores joguem um dado e tenham resultados aleatórios.
Guarde esses resultados em um dicionário.
No final, coloque esse dicionário em ordem, sabendo que o vencedor tirou o maior número no dado.

"""
from random import randrange
from time import sleep

sep = '┴' * 30
quant_jogadores = int(input('Quantos jogadores neste jogo: '))
print(sep)

jogadas = [randrange(1, 20 + 1) for i in range(quant_jogadores)]
resultados = {'Jogador ' + str(i + 1): value for i, value in enumerate(jogadas)}

for key, value in resultados.items():
    print(f'{key} tirou {value} no D20')
    sleep(1)

print(sep)
print('A inicitiva será: ')
for i, key in enumerate(sorted(resultados, key=resultados.get, reverse=True)):
    print(f'{i + 1}º: {key} com {resultados[key]}')
