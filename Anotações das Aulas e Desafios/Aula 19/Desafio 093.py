"""
DESAFIO 093: Dicionário em Python

Crie um programa que gerencie o aproveitamento de um jogador de futebol.
O nome do jogador e quantas partidas ele jogou.
Depois vai ler a quantidade de gols feitos em cada partida.
No final, tudo isso será guardado em um dicionário, incluindo o total de gols feitos durante o campeonato
"""

from random import randrange

jogador = dict()

jogador['nome'] = str(input('Nome: ')).strip().capitalize()
jogador['partidas'] = int(input('Partidas jogadas: '))

jogador['gol_partida'] = []
for i in range(jogador['partidas']):
    jogador['gol_partida'].append(randrange(3))

# for i in range(jogador['partidas']):
#     gol_partida_tmp = int(input(f'Gols marcados na {i + 1}ª partida: '))
#     jogador['gol_partida'].append(gol_partida_tmp)

jogador['gols_camp'] = sum(jogador['gol_partida'])

for key, value in jogador.items():
    print(f'{key}: {value}')
