"""
DESAFIO 094
Crie um programa que leia nome, sexo e idade de várias pessoas guardando os dados em um dicionário e todos os dicionários em uma lista.
No final, mostre:
A) Quantas pessoas foram cadastradas
B) A média de idade do grupo
C) Uma lista com todas as mulheres
D) Uma lista com todas as pessoas com idade acima da média
"""

sep = '░' * 50
quant_pessoas = int(input('Quantas pessoas deseja cadastrar? '))
pessoas, mulheres, idade_acima_media = [], [], []
idade_media = 0

for i in range(quant_pessoas):
    nome_tmp = str(input('Nome: '))
    while True:
        sexo_tmp = str(input('Sexo [M/F]: ')).strip().upper()
        if sexo_tmp in 'MF':
            break
        print('Inválido. Tente novamente')
    idade_tmp = int(input('Idade: '))
    i = {'nome': nome_tmp,
         'sexo': sexo_tmp,
         'idade': idade_tmp}
    pessoas.append(i)


for pessoa in pessoas:
    idade_media += pessoa['idade']
    if pessoa['sexo'] in 'fF':
        mulheres.append(pessoa)
idade_media = idade_media / len(pessoas)
for velho in pessoas:
    if velho['idade'] > idade_media:
        idade_acima_media.append(velho)

print(sep)
print(f'{len(pessoas)} pessoas foram cadastradas.\nA idade média do grupo: {idade_media}\nAs mulheres: {mulheres}\nAs pessoas com idade acima da média: {idade_acima_media}')
