"""
DESAFIO 095
Aprimore o desafio 093 para que ele funciona com vários jogadores, incluindo um sistema de visuaização de detalhes do aproveitamento de cada jogador
"""

from random import randrange, choices
import string

quant_jogadores = int(input('Quantos jogadores irá cadastrar? '))
time = []

for i in range(quant_jogadores):
    nome_tmp = str(input('Nome: ')).strip().capitalize()
    partidas_tmp = int(input('Partidas jogadas: '))
    gol_partida_tmp = []

    for i in range(partidas_tmp):
        gol_tmp = int(input(f'Gols marcados na {i + 1}ª partida: '))
        gol_partida_tmp.append(gol_tmp)

    gols_camp_tmp = sum(gol_partida_tmp)
    i = {'nome': nome_tmp,
         'partidas': partidas_tmp,
         'gol_partida': gol_partida_tmp,
         'gols_camp': gols_camp_tmp,
         'media_gol': round(gols_camp_tmp / partidas_tmp, 2)}
    time.append(i)

# for i in range(quant_jogadores):
#     nome_tmp = ''.join(choices(string.ascii_letters, k=6))
#     partidas_tmp = randrange(30)
#     gol_partida_tmp = []
#     for i in range(partidas_tmp):
#         gol_partida_tmp.append(randrange(3))

#     gols_camp_tmp = sum(gol_partida_tmp)
#     i = {'nome': nome_tmp,
#          'partidas': partidas_tmp,
#          'gol_partida': gol_partida_tmp,
#          'gols_camp': gols_camp_tmp,
#          'media_gol': round(gols_camp_tmp / partidas_tmp, 2)}
#     time.append(i)

for jogador in time:
    print(f"{jogador['nome']} jogou {jogador['partidas']} partidas, marcou {jogador['gols_camp']} gols no campeonado com uma média de {jogador['media_gol']} gols por jogo")
