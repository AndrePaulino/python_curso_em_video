"""
DESAFIO 069: Análise de Dados do Grupo

Crie um programa que leia a idade e o sexo de várias pessoas. A cada pessoa cadastrada,
o programa deverá perguntar se o usuário quer ou não continuar. No final, mostre:

A) Quantas pessoas têm mais de 18 anos.
B) Quantos homens foram cadastrados.
C) Quantas mulheres têm menos de 20 anos.
"""

# TODO: Subtituir dois arrays por dict

idade, sexo = [], []
cont, maior_18, tot_m, f_menor_20 = 0, 0, 0, 0
sep = '─' * 40
while True:
    sexo_tmp, continuar = '', ' '
    idade_tmp = 0

    titulo = f'PESSOA Nº {cont + 1}'
    print(sep)
    print(f'{titulo:^40}')
    print(sep)

    idade_tmp = int(input('Idade: '))
    idade.append(idade_tmp)

    sexo_tmp = str(input('Sexo [M/F]: ').strip().upper())
    while sexo_tmp not in ('M', 'F'):
        sexo_tmp = str(input('Sexo [M/F]: ')).strip().upper()
    sexo.append(sexo_tmp)
    print(sep)

    if idade_tmp > 18:
        maior_18 += 1
    if sexo_tmp == 'M':
        tot_m += 1
    else:
        if idade_tmp < 20:
            f_menor_20 += 1

    while continuar not in 'SN':
        continuar = str(input('Deseja registrar outra pessoas [S/N]? ')).strip().upper()
    if continuar == 'N':
        break
    cont += 1

print(sep)
print(f'Dos {cont + 1} cadastrados:\n{maior_18} pessoas são maiores de idade\n{tot_m} são homens\n{f_menor_20} são mulheres menores de 20 anos')
