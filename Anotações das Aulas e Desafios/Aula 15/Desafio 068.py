"""
DESAFIO 068: Jogo do Par ou Ímpar

Faça um programa que jogue par ou ímpar com o computador. O jogo só será interrompido quando o
jogador PERDER, mostrando o total de vitórias consecutivas que ele conquistou no final do jogo.
"""

from random import randint

while True:
    pc_escolha, player_escolha, par_impar = '', ' ', ''
    pc_mao, player_mao, total, player_vit = 0, 0, 0, 0

    while player_escolha not in 'PI':
        # print('[ P ] Par\n[ I ] Ímpar')
        player_escolha = str(input('[ P ] Par\n[ I ] Ímpar\n>>>>>>Escolha: ')).strip().upper()

    pc_escolha = 'P' if player_escolha == 'I' else 'I'

    player_mao = int(input('Qual valor? '))
    pc_mao = randint(0, 10)

    total = player_mao + pc_mao
    par_impar = 'P' if total % 2 == 0 else 'I'

    print('^' * 30)
    print(f'Você jogou {player_mao}, o computador {pc_mao}\nTotal de {total}, ', end='')
    print('PAR' if par_impar == 'P' else 'ÍMPAR')

    if par_impar == player_escolha:
        print('VOCÊ VENCEU!')
        player_vit += 1
    else:
        print('VOCÊ PERDEU!')
        break

    print('^' * 30)

print(f'Você venceu {player_vit} jogos!')
