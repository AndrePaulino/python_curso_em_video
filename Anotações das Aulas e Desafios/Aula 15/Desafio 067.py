"""
DESAFIO 067: Tabuada v3.0

Faça um programa que mostre a tabuada de vários números, um de cada vez, para cada valor digitado
pelo usuário. O programa será interrompido quando o número solicitado for negativo.
"""

print('Digite um número negativo para cancelar')
while True:
    num = int(input('Digite um número: '))
    print('-' * 20)
    if num < 0:
        break
    print(f'A tabuada de {num}:')
    for i in range(1, 11):
        print(f'{num} x {i} = {num * i}')
    print('-' * 20)
