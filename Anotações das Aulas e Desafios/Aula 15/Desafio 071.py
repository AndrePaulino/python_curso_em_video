"""
DESAFIO 071: Simulador de Caixa Eletrônico

Crie um programa que simule o funcionamento de um caixa eletrônico. No início, pergunte ao
usuário qual será o valor a ser sacado (número inteiro) e o programa vai informar quantas
cédulas de cada valor serão entregues.

OBS: Considere que o caixa possui cédulas de R$ 50, R$ 20, R$ 10 e R$ 1.
"""

valor_saque = int(input('Valor do saque: R$: '))
resto = valor_saque

cedulas = (50, 20, 10, 1)
saque = {key: None for key in cedulas}

print('=' * 20)
for nota in saque:
    saque[nota] = resto // nota
    resto = resto % nota
    print(f'{saque[nota]} notas de {nota}')
print('=' * 20)
