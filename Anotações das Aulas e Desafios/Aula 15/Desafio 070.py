"""
DESAFIO 070: Estatísticas em Produtos

Crie um programa que leia o nome e o preço de vários produtos. O programa
deverá perguntar se o usuário vai continuar. No final, mostre:

A) Qual é o total gasto na compra.
B) Quantos produtos custam mais de R$ 1000.
C) Qual é o nome do produto mais barato.
"""

# TODO: Subtituir dois arrays por dict

produtos, precos = [], []
cont, prod_mais_mil, preco_mais_barato, tot_compra = 0, 0, 0, 0
prod_mais_barato = ''

while True:
    prod_tmp, continuar = '', ' '
    preco_tmp = 0

    prod_tmp = str(input('Nome do produto: '))
    preco_tmp = float(input('Preço do produto: R$ '))
    produtos.append(prod_tmp)
    precos.append(preco_tmp)

    tot_compra += preco_tmp

    if preco_tmp > 1000:
        prod_mais_mil += 1

    if preco_tmp < preco_mais_barato or cont == 0:
        preco_mais_barato = preco_tmp
        prod_mais_barato = prod_tmp

    while continuar not in 'SN':
        continuar = str(input('Gostaria de continuar [S/N]? ')).strip().upper()
    if continuar == 'N':
        break

    cont += 1

print(f'O total da compra: R${tot_compra}\n{prod_mais_mil} custam mais de R$1000\n{prod_mais_barato} é o produto mais barato')
