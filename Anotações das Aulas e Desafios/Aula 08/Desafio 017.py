"""
DESAFIO 017: Catetos e Hipotenusa

Faça um programa que leia o comprimento do cateto oposto e do cateto adjacente
de um triângulo retângulo, calcule e mostre o comprimento da hipotenusa.
"""

from math import hypot

print('De um triângulo retângulo, digite o comprimento do...')
cat_x = float(input('cateto oposto: '))
cat_y = float(input('cateto adacente: '))

hipo = hypot(cat_x, cat_y)

print ('O comprimento de sua hipotenusa é: {:.2f}'.format(hipo))
