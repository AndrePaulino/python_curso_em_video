"""
DESAFIO 020: Sorteando uma Ordem na Lista

O mesmo professor do desafio anterior quer sortear a ordem de apresentação de trabalhos dos alunos.
Faça um programa que leia o nome dos quatro alunos e mostre a ordem sorteada.
"""

from random import shuffle

alunos = ['Zé', 'Zéca', 'Zécama', 'Zécamargo']
shuffle(alunos)

print('A ordem de apresentenção será: {}'.format(alunos))
