"""
DESAFIO 018: Seno, Cosseno e Tangente

Faça um programa que leia um ângulo qualquer e mostre na tela o valor do seno, cosseno e tangente desse ângulo.
"""

from math import sin, cos, atan, degrees, radians

user_ang = float(input('Digite um ângulo qualquer: '))
ang = radians(int((user_ang % 360)))
sin = sin(ang)
cos = cos(ang)
atan = atan(ang)

print('Do ângulo {}.\nSeu seno: {:.3f}\nSeu cosseno: {:.3f}\nSua tangente: {:.3f}'.format(degrees(ang), sin, cos, atan))
