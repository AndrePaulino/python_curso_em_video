"""
DESAFIO 022: Analisador de Textos

Crie um programa que leia o nome completo de uma pessoa e mostre:

> O nome com todas as letras maiúsculas e minúsculas.
> Quantas letras possui (sem considerar espaços).
> Quantas letras tem o primeiro nome.
"""

nome = str(input('Escreva um nome completo: ')).strip()
letras_tot = len(nome) - nome.count(' ')

print('{}\n{}\nEsse nome tem {} letras e o primeiro nome {}'.format(nome.upper(), nome.lower(), letras_tot, nome.find(' ')))