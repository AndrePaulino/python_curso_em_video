"""
DESAFIO 023: Separando Dígitos de um Número

Faça um programa que leia um número de 0 a 9999 e mostre na tela cada um dos dígitos separados.

Ex:
Digite um número: 1834
Unidade: 4
Dezena: 3
Centena: 8
Milhar: 1
"""

num = int(input('Digite um número entre 0 e 9999: '))
uni = (num // 1) % 10
dez = (num // 10) % 10
cen = (num // 100) % 10
mil = (num // 1000) % 10

print('Unidade: {}\nDezena: {}\nCentena: {}\nMilhar: {}'.format(uni, dez, cen, mil))