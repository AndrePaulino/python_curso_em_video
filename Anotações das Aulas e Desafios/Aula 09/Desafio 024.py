"""
DESAFIO 024: Verificando as Primeiras Letras de um Texto

Crie um programa que leia o nome de uma cidade e diga se ela começa ou não com o nome "SANTO".
"""

cidade = str(input('Digite o nome de uma cidade: ')).upper().strip()

print('{} possui Santo no nome: {}'.format(cidade.title(), 'SANTO' in cidade))
