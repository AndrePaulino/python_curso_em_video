"""
DESAFIO 027: Primeiro e Último Nome de uma Pessoa

Faça um programa que leia o nome completo de uma pessoa, mostrando em seguida o primeiro
e o último nome separadamente.

Ex: Ana Maria de Souza
Primeiro = Ana
Último = Souza
"""

nome = str(input('Digite um nome completo: ')).strip()
prim_nome = nome[:nome.find(' ')]
ult_nome = nome[nome.rfind(' '):len(nome)]

print('Primeiro nome: {}\nÚltimo: {}'.format(prim_nome, ult_nome))