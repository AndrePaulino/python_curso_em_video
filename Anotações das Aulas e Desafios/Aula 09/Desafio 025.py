"""
DESAFIO 025: Procurando uma String Dentro de Outra

Crie um programa que leia o nome de uma pessoa e diga se ela tem "SILVA" no nome.
"""

nome = str(input('Digite um nome: ')).upper().strip()

print('{} possui Silva no nome: {}'.format(nome.title(), 'SILVA' in nome))
